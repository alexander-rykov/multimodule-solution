﻿using System.Text;
using NUnit.Framework;

namespace Module2.Tests
{
    [TestFixture]
    public class MyClassTests
    {
        [Test]
        public void ReadFromString()
        {
            string text = "1" + Environment.NewLine +
                "2" + Environment.NewLine +
                "3" + Environment.NewLine +
                "4" + Environment.NewLine +
                "5";

            using StringReader reader = new StringReader(text);

            long actualResult = MyClass.SumValuesInStreamReader(reader);
            Assert.AreEqual(15, actualResult);
        }

        [Test]
        public void ReadFromFile()
        {
            const string filename = "values.txt";

            using StreamReader reader = new StreamReader(filename, Encoding.UTF8, true);

            long actualResult = MyClass.SumValuesInStreamReader(reader);
            Assert.AreEqual(45, actualResult);
        }
    }
}
