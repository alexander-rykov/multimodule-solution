﻿using NUnit.Framework;

namespace Module1.Tests
{
    [TestFixture]
    public class MyClassTests
    {
        [TestCase(1, ExpectedResult = 1)]
        [TestCase(2, ExpectedResult = 2)]
        [TestCase(3, ExpectedResult = 3)]
        public int ReturnValue_ReturnsValue(int n)
        {
            return MyClass.ReturnValue(n);
        }
    }
}
