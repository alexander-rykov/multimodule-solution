﻿using System.Globalization;

namespace Module2
{
    public static class MyClass
    {
        public static long SumValuesInStreamReader(TextReader reader)
        {
            long sum = 0;

            string? line;
            while ((line = reader.ReadLine()) != null)
            {
                int value = int.Parse(line, CultureInfo.InvariantCulture);
                sum += value;
            }

            return sum;
        }
    }
}
